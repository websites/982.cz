#!/usr/bin/python2
# -*- coding: utf-8 -*-

from cz982 import check_server
import cgi

args = cgi.FieldStorage()
json = check_server.check(args["web"].value)

print "Content-Type: application/json"
print "Access-Control-Allow-Origin: *"
print
print json