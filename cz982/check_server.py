#!/usr/bin/python2
# -*- coding: utf-8 -*-

from subprocess import call
from settings_production import ROOT_KEY
import re
import unbound

def check(url):
    r = '{ '
    urlregex = re.compile("^(https?\://)?[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(/\S*)?$")
    if urlregex.match(url):
        r += '"ipv6": '
        if call(["wget", "-q", "-O/dev/null", "-6", url]) == 0:
            r += 'true'
        else:
            r += 'false'
        r += ', '
        r += '"dnssec": '    
        ctx = unbound.ub_ctx()
        ctx.set_option('auto-trust-anchor-file:', ROOT_KEY)
        domain = url.replace("http://","").replace("https://","").replace("/","")       
        status, result = ctx.resolve(domain, rrtype=unbound.RR_TYPE_SOA)
        if status == 0 and result.secure:
            r += 'true'
        else:
            r += 'false'
    else:
        r += '"error": "not a valid url/domain"'    
    r += ' }'
    
    return r