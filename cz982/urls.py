from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.http import HttpResponseRedirect
from django.conf.urls.i18n import i18n_patterns
from django.contrib.sitemaps.views import sitemap
from django.contrib.sitemaps import FlatPageSitemap, GenericSitemap
from testapp.models import Result
from django.utils.translation import ugettext_lazy as _

sitemaps = {
    'flatpages': FlatPageSitemap,
}

urlpatterns = patterns(
    '',
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap'),
    url(r'^i18n/', include('django.conf.urls.i18n')),
    url(r'^admin/', include(admin.site.urls)),
)


urlpatterns += i18n_patterns(
    '',
    url(r'^t/', include('testapp.urls')),
    url(r'^login/$', 'django.contrib.auth.views.login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout'),
    url(r'^p/', include('django.contrib.flatpages.urls')),
    url(r'^$', lambda r: HttpResponseRedirect('/p/index/')),
)

admin.site.site_header = _("982.cz administration")
admin.site.site_title = _("IPv6 &amp; DNSSEC support test")
