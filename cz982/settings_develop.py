from .settings import *

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "hl3p*+-86$(q0!#yz*%*g+i(&77(zd28g25&)9uy%(iquubgin"

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": "982",
    }
}

# SECURITY WARNING: don"t run with debug turned on in production!
DEBUG = True
TEMPLATE_DEBUG = True