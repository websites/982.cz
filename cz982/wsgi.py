"""
WSGI config for cz982 project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/howto/deployment/wsgi/
"""

import os
from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "cz982.settings")
os.environ["DJANGO_SETTINGS_MODULE"] = "cz982.settings_develop"

application = get_wsgi_application()
