from django.conf.urls import patterns, url
from django.views.generic.base import TemplateView
from django.http import HttpResponseRedirect

from testapp import views

urlpatterns = patterns(
    '',
    url(r'^run/success/', TemplateView.as_view(template_name='testapp/success.html')),
    url(r'^run/', views.CreateResult.as_view()),
    url(r'^list/', views.ListResults),
    (r'^$', lambda r: HttpResponseRedirect('/t/run/')),
)
