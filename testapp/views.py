from django.views.generic.edit import CreateView
from django.http import HttpResponse
from testapp.models import Result, Website
from django.contrib.flatpages.models import FlatPage
from django.utils.safestring import mark_safe
from django.shortcuts import redirect
from django.template import RequestContext, loader


class CreateResultMixin(object):
    """
    Retrieves the FlatPage object for the specified url, and includes it in the
    context.

    If no url is specified, request.path is used.
    """

    url = None

    def get_context_data(self, **kwargs):
        if not self.url:
            self.url = self.request.path

        context = super(CreateResultMixin, self).get_context_data(**kwargs)
        try:
            flatpage = FlatPage.objects.get(url=self.url)
            flatpage.title = mark_safe(flatpage.title)
            flatpage.content = mark_safe(flatpage.content)
            context["flatpage"] = flatpage
        except FlatPage.DoesNotExist:
            context["flatpage"] = None

        if self.request.user.is_authenticated():
            context["website"] = Website.objects.get(owner=self.request.user)

        return context


def ListResults(request):
    if not request.user.is_authenticated():
        return redirect('/login/?next=%s' % request.path)

    if request.user.is_staff:
        p = Result.objects.order_by('-test_date')
    else:
        # request.user.id
        # p = Result.objects.order_by('-test_date')[:5]
        p = Result.objects.filter(owner=request.user)

    template = loader.get_template('testapp/result_list.html')
    context = RequestContext(request, {
        'results': p,
    })

    return HttpResponse(template.render(context))


class CreateResult(CreateResultMixin, CreateView):
    model = Result
    fields = [
            'ipv4_address',
            'ipv6_address',
            'has_dnssec',
            'www_url',
            'www_ipv4',
            'www_ipv6',
            'www_dnssec',
            'ipv4_upload',
            'ipv4_download',
            'ipv6_upload',
            'ipv6_download',
            'owner'
            ]
    success_url = "/p/success/"
