from django.contrib import admin
from testapp.models import Result, Website
from django.contrib.admin import DateFieldListFilter, BooleanFieldListFilter
from django.utils.translation import ugettext_lazy as _


class ResultAdmin(admin.ModelAdmin):
    """
    Defines fieldsets for the admin "result" page.
    """
    fieldsets = [
        (None,                {'fields': ['owner']}),
        (_('IP addresses'),   {'fields': ['ipv4_address', 'ipv6_address']}),
        (_('DNSSEC support'),         {'fields': ['has_dnssec']}),
        (_('Speed'),          {'fields': ['ipv4_download', 'ipv4_upload',
                                          'ipv6_download', 'ipv6_upload'],
                               'classes': ['collapse']}),
        (_('Website'),        {'fields': ['www_url', 'www_ipv4', 'www_ipv6', 'www_dnssec'],
                               'classes': ['collapse']}),
    ]
    list_display = ('test_date', 'owner', 'has_ipv6', 'has_dnssec', 'www_url', 'www_ipv6', 'www_dnssec')
    list_filter = (
        ('test_date', DateFieldListFilter),
        ('ipv6_address', BooleanFieldListFilter),
        ('has_dnssec', BooleanFieldListFilter),
        ('www_ipv6', BooleanFieldListFilter),
        ('www_dnssec', BooleanFieldListFilter),
    )
    search_fields = ['www_url']
    
class WebsiteAdmin(admin.ModelAdmin):
    """
    Defines fieldsets for the admin "Website" page.
    """
    list_display = ('owner', 'www_url')
    search_fields = ['www_url']

admin.site.register(Result, ResultAdmin)
admin.site.register(Website, WebsiteAdmin)
