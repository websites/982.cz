from django.db import models
from django.contrib.auth.models import User
from django.utils.formats import date_format
from django.utils.translation import ugettext_lazy as _


class Website(models.Model):
    """
    Website pairing for user.
    """
    www_url = models.CharField(null=False, max_length=255, verbose_name=_("Website URL"))
    owner = models.OneToOneField(User, blank=False, db_index=True, verbose_name=_("User"))

    def __str__(self):
        return u'%s' % (self.www_url)

    class Meta:
        verbose_name = _("Website")
        verbose_name_plural = _("Websites")


class Result(models.Model):
    """
    Finished test result, saved by JS after running tests.
    """
    test_date = models.DateTimeField(db_index=True, auto_now_add=True, verbose_name=_("Test date and time"))
    ipv4_address = models.GenericIPAddressField(protocol="IPv4", blank=True, null=True, verbose_name=_("IPv4 address"))
    ipv6_address = models.GenericIPAddressField(protocol="IPv6", blank=True, null=True, verbose_name=_("IPv6 address"))
    has_dnssec = models.BooleanField(default=False, verbose_name=_("DNSSEC support"))
    www_url = models.CharField(null=False, max_length=255, verbose_name=_("Website URL"))
    www_ipv4 = models.BooleanField(default=True, verbose_name=_("Website supports IPv4"))
    www_ipv6 = models.BooleanField(default=False, verbose_name=_("Website supports IPv6"))
    www_dnssec = models.BooleanField(default=False, verbose_name=_("Website supports DNSSEC"))
    ipv4_upload = models.FloatField(blank=True, null=True)
    ipv4_download = models.FloatField(blank=True, null=True)
    ipv6_upload = models.FloatField(blank=True, null=True)
    ipv6_download = models.FloatField(blank=True, null=True)
    owner = models.ForeignKey(User, unique=False, blank=True, db_index=True, verbose_name=_("User"))

    def has_ipv4(self):
        return True if self.ipv4_address is not None else False

    has_ipv4.boolean = True
    has_ipv4.short_description = _("IPv4 connectivity")

    def has_ipv6(self):
        return True if self.ipv6_address is not None else False

    has_ipv6.boolean = True
    has_ipv6.short_description = _("IPv6 connectivity")

    def __str__(self):
        return u'%s %s' % (self.owner, date_format(self.test_date, "SHORT_DATETIME_FORMAT"))

    class Meta:
        verbose_name = _("Test result")
        verbose_name_plural = _("Test results")
