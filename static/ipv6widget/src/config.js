/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// debugging area visible
CZNIC_WIDGET.DEBUG = false;

CZNIC_WIDGET.IP4 = 'ip4';
CZNIC_WIDGET.IP6 = 'ip6';

// language to use when desired is not available
CZNIC_WIDGET.FALLBACK_LANGUAGE = "en";

// links to the explanations of the states
CZNIC_WIDGET.IP6_GREEN_DNSSEC_GREEN_LINK = "http://labs.nic.cz/page/960/";
CZNIC_WIDGET.IP6_GREEN_DNSSEC_RED_LINK = "http://labs.nic.cz/page/963/";
CZNIC_WIDGET.IP6_GREY_DNSSEC_GREEN_LINK = "http://labs.nic.cz/page/961/";
CZNIC_WIDGET.IP6_GREY_DNSSEC_RED_LINK = "http://labs.nic.cz/page/964/";
CZNIC_WIDGET.IP6_RED_DNSSEC_GREEN_LINK = "http://labs.nic.cz/page/962/";
CZNIC_WIDGET.IP6_RED_DNSSEC_RED_LINK = "http://labs.nic.cz/page/967/";
/*
 * Tests IDs
 */
CZNIC_WIDGET.IP4_TEST_ID = "ip4";
CZNIC_WIDGET.IP6_DNS_TEST_ID = "ip6dns";
CZNIC_WIDGET.IP6_NODNS_TEST_ID = "ip6nodns";
CZNIC_WIDGET.IP6_JUMBO_TEST_ID = "ip6jumbo";
CZNIC_WIDGET.IP6_DUAL_TEST_ID = "ip6dual";
CZNIC_WIDGET.DNSSEC_TEST_ID = "dnssec"
CZNIC_WIDGET.IP4_DOWN_TEST_ID = "down4";
CZNIC_WIDGET.IP4_UP_TEST_ID = "up4";
CZNIC_WIDGET.IP6_DOWN_TEST_ID = "down6";
CZNIC_WIDGET.IP6_UP_TEST_ID = "up6";

CZNIC_WIDGET.IPV6_OVERALL_UPDATED = false;

/*
 * URIs for ipv6 and connection speed tests
 */

// upload test uris
CZNIC_WIDGET.UPLOAD_TEST_URL_IP4 = "//ipv4.test-ipv6.cz/ipv6widget/upload.php";
CZNIC_WIDGET.UPLOAD_TEST_URL_IP6 = "//ipv6.test-ipv6.cz/ipv6widget/upload.php";

// download test uris
CZNIC_WIDGET.DOWNLOAD_TEST_URL_IP4 = "//ipv4.test-ipv6.cz/ip/?callback=?&size=";
CZNIC_WIDGET.DOWNLOAD_TEST_URL_IP6 = "//ipv6.test-ipv6.cz/ip/?callback=?&size=";

// ipv4 only test
CZNIC_WIDGET.IP4_ONLY_TEST = "//ipv4.test-ipv6.cz/ip/?callback=?";

// ipv6 tests
CZNIC_WIDGET.IP6_DNS_TEST = "//ipv6.test-ipv6.cz/ip/?callback=?";
CZNIC_WIDGET.IP6_NO_DNS_TEST = "//[2001:1488:800:400::2:120]:80/ip/?callback=?";
CZNIC_WIDGET.IP6_JUMBO_TEST = "//ipv6.test-ipv6.cz/ip/?callback=?&size="; 
CZNIC_WIDGET.IP6_DUALSTACK_TEST = "//ds.test-ipv6.cz/ip/?callback=?";

// DNSSEC test
CZNIC_WIDGET.DNSSEC_TEST = "//badsig.dnssec.test-ipv6.cz/ip/?callback=?";

// bytes to test ipv6 jumbo packets
CZNIC_WIDGET.JUMBO_TEST_LEN = 1600;

// HTML settings
CZNIC_WIDGET.CSS_SPEED_RESULT_CONTAINER_LM = "34px";
CZNIC_WIDGET.CSS_DNSSEC_IP6_TEXT_LM = "34px";


CZNIC_WIDGET.WIDGET_ID = "ipv6-dnssec-widget";
CZNIC_WIDGET.START_BUTTON_ID = "test_starter";
CZNIC_WIDGET.RESTART_BUTTON_ID = "test_restarter";
