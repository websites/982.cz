/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Defines available tests and runs the tests when the page is loaded.
 * 
 */

CZNIC_WIDGET.IPV6_OK = false;
CZNIC_WIDGET.IP6 = "6";
CZNIC_WIDGET.IP4 = "4";

// structure: test function, test_id, id of html element where the result is written, test_url
CZNIC_WIDGET.available_tests = [
	[CZNIC_WIDGET.ipv4_only_test,
			CZNIC_WIDGET.IP4_TEST_ID,
			"ip4_info",
			CZNIC_WIDGET.IP4_ONLY_TEST],
	[CZNIC_WIDGET.ipv6_dns_test,
			CZNIC_WIDGET.IP6_DNS_TEST_ID,
			"ip6_dns_info",
			CZNIC_WIDGET.IP6_DNS_TEST ],
	/*[CZNIC_WIDGET.ipv6_nodns_test,
			CZNIC_WIDGET.IP6_NODNS_TEST_ID,
			"ip6_nodns_info",
			CZNIC_WIDGET.IP6_NO_DNS_TEST],*/
	[CZNIC_WIDGET.ipv6_jumbo_test,
			CZNIC_WIDGET.IP6_JUMBO_TEST_ID,
			"ip6_jumbo_info",			
			CZNIC_WIDGET.IP6_JUMBO_TEST + CZNIC_WIDGET.JUMBO_TEST_LEN.toString()+"&fill="+CZNIC_WIDGET.fill('j', CZNIC_WIDGET.JUMBO_TEST_LEN)],
	[CZNIC_WIDGET.ipv6_dualstack_test,
			CZNIC_WIDGET.IP6_DUAL_TEST_ID,
			"ip6_dualstack_info",
			CZNIC_WIDGET.IP6_DUALSTACK_TEST],
	[CZNIC_WIDGET.dnssec_test,
			CZNIC_WIDGET.DNSSEC_TEST_ID, 
			"dnssec_info",
			CZNIC_WIDGET.DNSSEC_TEST]
	//[CZNIC_WIDGET.run_speed_tests, "", "", ""],

];

// test count is the sum of ipv4/6 tests, dnssec test and 4 upload/download tests
CZNIC_WIDGET.TEST_COUNT = CZNIC_WIDGET.available_tests.length + 3;
CZNIC_WIDGET.TESTS_DONE = 0;

CZNIC_WIDGET.dequeued = [];


CZNIC_WIDGET.run_tests = function (){	
	for (var i=0; i< CZNIC_WIDGET.available_tests.length; i++){
		var settings = CZNIC_WIDGET.available_tests[i];
		// copy test setting into other queue
		CZNIC_WIDGET.dequeued.push(settings.slice(0));		
		var fn = settings.shift();
		fn.apply(this, settings);		
	}
		
}

CZNIC_WIDGET.start = function (){
	var context = CZNIC_WIDGET.find_context_element();
	if (context == null){
		return;
	}
	// set the context of widget not to modify rest of document
	CZNIC_WIDGET.widget_context = context;	
	// hide the start button
	$("#"+ CZNIC_WIDGET.START_BUTTON_ID, CZNIC_WIDGET.widget_context).hide();	
	
	// and lets go
	CZNIC_WIDGET.run_tests();	
	// restart button not needed
	//CZNIC_WIDGET.show_controls();
}

CZNIC_WIDGET.restart = function (){
	$("#"+CZNIC_WIDGET.RESTART_BUTTON_ID, CZNIC_WIDGET.widget_context).hide();	
	CZNIC_WIDGET.TESTS_DONE = 0;
	CZNIC_WIDGET.test_results = [];	
	//switch available and dequeued tests
	CZNIC_WIDGET.available_tests = CZNIC_WIDGET.dequeued;
	CZNIC_WIDGET.dequeued = []
	// empty the download/upload results arrays 
	CZNIC_WIDGET.download_results = [];
	CZNIC_WIDGET.upload_results = [];
	
	CZNIC_WIDGET.download_results_v6 = [];
	CZNIC_WIDGET.upload_results_v6 = [];
	//reset the results array for reference rtt
	CZNIC_WIDGET.ref_rtts = [];
	CZNIC_WIDGET.ref_rtts_v6 = [];
	
	CZNIC_WIDGET.REF_RTT = 0;
	CZNIC_WIDGET.REF_RTT_V6 = 6;
	
	CZNIC_WIDGET.ERR_EMPTY_RTTS = 0;
	CZNIC_WIDGET.ERR_EMPTY_RTTS_V6 = 0
	
	CZNIC_WIDGET.UP_TEST_LEVEL = 0;
	CZNIC_WIDGET.DOWN_TEST_LEVEL = 0;
	CZNIC_WIDGET.UP_TEST_LEVEL_V6 = 0;
	CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 = 0;
	
	CZNIC_WIDGET.IPV6_OVERALL_UPDATED = false;
	// and lets go	
	CZNIC_WIDGET.run_tests();		
}

CZNIC_WIDGET.set_labels = function(){
	$("#header_label").html(CZNIC_WIDGET.gettext(CZNIC_WIDGET.HEADER_LABEL));
	$("#dnssec_label").html(CZNIC_WIDGET.gettext(CZNIC_WIDGET.DNSSEC_LABEL));
	$("#ipv6_label").html(CZNIC_WIDGET.gettext(CZNIC_WIDGET.IPV6_LABEL));
	$("#speed_tests_btn").html(CZNIC_WIDGET.gettext(CZNIC_WIDGET.CONSPEED_LABEL));
}

CZNIC_WIDGET.get_http_param = function(param_name){
	name = param_name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}

CZNIC_WIDGET.detect_lang = function(){
	var lang = CZNIC_WIDGET.get_http_param("lang");
	lang = lang.substring(0,2);
	// if language is not specified or is not supported, use browser lang
	if ((lang== "") || ($.inArray(lang, CZNIC_WIDGET.SUPPORTED_LANGS) == -1)){		
		lang = (navigator.language) ? navigator.language : navigator.userLanguage;
		lang = lang.substring(0,2);		
	}
	// if browser lang is not supported, use 'en'
	if ($.inArray(lang, CZNIC_WIDGET.SUPPORTED_LANGS) == -1) {
  	lang = CZNIC_WIDGET.FALLBACK_LANGUAGE;
  }	
	return lang;
}

CZNIC_WIDGET.set_lang = function(lang){
	$("#language").html(lang);	
}

$(document).ready(function(){	
	// show and hide the details div
	$("#details").hide();
	$("#widget_show_details").change(function(){
		if ($(this).is(":checked")){
			$("#details").show();
		}
		else{
			$("#details").hide();
		}
	})	
	$("#widget_show_details").trigger('change');
	
	if (CZNIC_WIDGET.DEBUG){
		$("#dbg_area").show();
		$("#test_controls").show();
	}
	else{
		$("#dbg_area").hide();
		$("#test_controls").hide();
	}
	
	// detect language set by user
	var lang = CZNIC_WIDGET.detect_lang();
	CZNIC_WIDGET.set_lang(lang);
	CZNIC_WIDGET.set_labels();
	
	// show/hide speed test
	var speedtest = CZNIC_WIDGET.get_http_param("speedtest")
	if ((speedtest == "off")){
		// hide speed test and show footer
		$("#speed_tests_control").hide();
		$("#footer").show();
	}
	else{
		// let the speed test visible and hide footer
		$("#footer").hide();
	}
	
	// ipv6 and DNSSEC test start when document ready
	CZNIC_WIDGET.start();
	// speed tests start on button click
	$("#speed_tests_btn").click(function(){
		// hide button
		$("#speed_tests_control").hide();		
		// show html for results
		$("#speed_tests").show();
		// run the tests
		CZNIC_WIDGET.run_speed_tests();
	});
	
	$("#speed_tests_btn").mousedown(function(){		
		$(this).addClass("pressed");
		}
	);
	
	$("#speed_tests_btn").mouseup(function(){		
		$(this).removeClass("pressed");
		}
	);
	
});


