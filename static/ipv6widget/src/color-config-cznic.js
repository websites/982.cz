/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
 * Color configuration - CZ.NIC version
 */

CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR = "white";

CZNIC_WIDGET.IMG_LOADER = "img-cznic/ajax-loader.gif";

CZNIC_WIDGET.IMG_DNSSEC_OK = "img-cznic/greenlight_dnssec.png";
CZNIC_WIDGET.IMG_DNSSEC_FAIL = "img-cznic/redlight_dnssec.png";

CZNIC_WIDGET.IMG_IP6_OK = "img-cznic/greenlight_ipv6.png";
CZNIC_WIDGET.IMG_IP6_FAIL = "img-cznic/redlight_ipv6.png";
CZNIC_WIDGET.IMG_IP6_OFF = "img-cznic/nolight_ipv6.png";

CZNIC_WIDGET.IMG_IP4_SPEED_DONE = "img-cznic/speedometer_rychlost1.png";
CZNIC_WIDGET.IMG_IP6_SPEED_DONE = "img-cznic/speedometer_rychlost2.png";