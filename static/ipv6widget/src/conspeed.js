/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
 *
 *    This program is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    This program is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*
 * This module contains all code that is used to test the connection speed.
 */

CZNIC_WIDGET.DOWN_INFO_DIV = "down_info_v4";
CZNIC_WIDGET.DOWN_INFO_DIV_V6 = "down_info_v6";
CZNIC_WIDGET.UP_INFO_DIV = "up_info_v4";
CZNIC_WIDGET.UP_INFO_DIV_V6 = "up_info_v6";

CZNIC_WIDGET.download_results = [];
CZNIC_WIDGET.upload_results = [];

CZNIC_WIDGET.download_results_v6 = [];
CZNIC_WIDGET.upload_results_v6 = [];

CZNIC_WIDGET.UPLOAD = "up";
CZNIC_WIDGET.DOWNLOAD = "down";

// url to send the requests with size 0
CZNIC_WIDGET.REF_URI = "http://ipv4.test-ipv6.cz/ip/?callback=?&size=0";
CZNIC_WIDGET.ref_rtts = [];
CZNIC_WIDGET.REF_RTT = 0;

// url to send the requests with size 0
CZNIC_WIDGET.REF_URI_V6 = "http://ipv6.test-ipv6.cz/ip/?callback=?&size=0";
CZNIC_WIDGET.ref_rtts_v6 = [];
CZNIC_WIDGET.REF_RTT_V6 = 0;

// used to measure the rtt with empty request
CZNIC_WIDGET.MAX_NUM_OF_EMPTY_REQ = 100;
// counter for error requests
CZNIC_WIDGET.ERR_EMPTY_RTTS = 0;
CZNIC_WIDGET.ERR_EMPTY_RTTS_v6 = 0;
CZNIC_WIDGET.MAX_ERR_EMPTY_RTTS = 10;


CZNIC_WIDGET.DOWN_TEST_LEVEL = 0;
CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 = 0;
CZNIC_WIDGET.DOWN_TEST_SIZES = [10000, 20000,
    50000, 100000,
    200000, 500000,
    1000000, 2000000,
    4000000, 8000000,
    16000000, 32000000,
    64000000
];
CZNIC_WIDGET.DOWN_TEST_MAX_LEVEL = (CZNIC_WIDGET.DOWN_TEST_SIZES.length - 1);
CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL = 3;
CZNIC_WIDGET.DOWN_TEST_TIME_LIMIT = 300;

CZNIC_WIDGET.UP_TEST_LEVEL = 0;
CZNIC_WIDGET.UP_TEST_LEVEL_V6 = 0;
CZNIC_WIDGET.UP_TEST_SIZES = [CZNIC_WIDGET.s10kB, CZNIC_WIDGET.s20kB,
    CZNIC_WIDGET.s50kB, CZNIC_WIDGET.s100kB,
    CZNIC_WIDGET.s1MB, CZNIC_WIDGET.s2MB,
    CZNIC_WIDGET.s5MB, CZNIC_WIDGET.s10MB
];
CZNIC_WIDGET.UP_TEST_MAX_LEVEL = (CZNIC_WIDGET.UP_TEST_SIZES.length - 1);
CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL = 3;
CZNIC_WIDGET.UP_TEST_TIME_LIMIT = 300;

// units
CZNIC_WIDGET.Bi = "bps";
CZNIC_WIDGET.By = "Bps";
CZNIC_WIDGET.kBi = "kbps";
CZNIC_WIDGET.MBi = "Mbps";
CZNIC_WIDGET.kBy = "kBps";
CZNIC_WIDGET.MBy = "MBps";

CZNIC_WIDGET.calculate_speed = function(bytes, miliseconds, unit) {
    /*
     * Returns object with speed and unit (o.speed and o.unit).
     * If unit parameter is undefined, best unit is chosen
     * (for values between 10^3 and 10^6 kbps, for over 10^6 bps Mbps)
     */
    var result = {};
    // limits for kilobytes and megabytes
    var MB_LIMIT = 1000000;
    var KB_LIMIT = 1000;

    if (unit == undefined) {
        // calculate speed in bps
        var speed_ps = bytes * 8 / (miliseconds / 1000);

        if (speed_ps <= KB_LIMIT) {
            result.speed = CZNIC_WIDGET.round_digits(speed_ps, 1);
            result.unit = CZNIC_WIDGET.Bi;
        }
        if (KB_LIMIT < speed_ps && speed_ps <= MB_LIMIT) {
            result.speed = CZNIC_WIDGET.round_digits(speed_ps / (1024), 1);
            result.unit = CZNIC_WIDGET.kBi;
        }
        if (MB_LIMIT <= speed_ps) {
            result.speed = CZNIC_WIDGET.round_digits(speed_ps / (1024 * 1024), 1);
            result.unit = CZNIC_WIDGET.MBi;
        }
    } else { //user defined unit
        var elements = bytes;
        if ((unit == CZNIC_WIDGET.Bi) ||
            (unit == CZNIC_WIDGET.kBi) ||
            (unit == CZNIC_WIDGET.MBi)) {
            elements = elements * 8;
        }
        result.unit = unit;
        // calculate speed in bps
        var speed_ps = elements / (miliseconds / 1000);
        if (unit == CZNIC_WIDGET.Bi || unit == CZNIC_WIDGET.By) {
            result.speed = speed_ps;
        }
        if (unit == CZNIC_WIDGET.kBi || unit == CZNIC_WIDGET.kBy) {
            result.speed = CZNIC_WIDGET.round_digits(speed_ps / (1024), 1);
        }

        if (unit == CZNIC_WIDGET.MBi || unit == CZNIC_WIDGET.MBy) {
            result.speed = CZNIC_WIDGET.round_digits(speed_ps / (1024 * 1024), 1);
        }

    }
    return result;
}

CZNIC_WIDGET.update_speed_result = function(action, status, time, size, protocol) {
    /*
     * Stores object with the result of speed test into global array.
     * Arrays for download and upload tests are separated.
     * Object stores:
     * - status - success or error
     * - time - time to complete request
     * - size - numbuer of bytes send/received
     * - reason - contains error reason (if any)
     */
    var o = new Object();
    o.status = status;
    o.time = time;
    o.size = size;
    o.reason = "";

    if (action == CZNIC_WIDGET.UPLOAD) {
        if (protocol == CZNIC_WIDGET.IP4) {
            CZNIC_WIDGET.upload_results.push(o);
            setTimeout(function() {
                $(".loader-anim.speed-ipv4").addClass("hidden");
            }, 700);
        } else {
            CZNIC_WIDGET.upload_results_v6.push(o);
            setTimeout(function() {
                $(".loader-anim.speed-ipv6").addClass("hidden");
            }, 700);
        }
    }
    if (action == CZNIC_WIDGET.DOWNLOAD) {
        if (protocol == CZNIC_WIDGET.IP4) {
            CZNIC_WIDGET.download_results.push(o);
        } else {
            CZNIC_WIDGET.download_results_v6.push(o);
        }
    }
}

CZNIC_WIDGET.construct_uri = function(action, num_bytes, protocol) {
    /*
     * Returns uri according to action.
     * If 'download', sets size parameter
     * If 'upload', returns uri for upload script
     */
    if (action == CZNIC_WIDGET.UPLOAD) {
        /*
         * For upload, use the predefined strings.
         */
        if (protocol == CZNIC_WIDGET.IP4) {
            return CZNIC_WIDGET.UPLOAD_TEST_URL_IP4;
        } else {
            return CZNIC_WIDGET.UPLOAD_TEST_URL_IP6;
        }
    } else {
        if (protocol == CZNIC_WIDGET.IP4) {
            return CZNIC_WIDGET.DOWNLOAD_TEST_URL_IP4 + num_bytes.toString();
        } else {
            return CZNIC_WIDGET.DOWNLOAD_TEST_URL_IP6 + num_bytes.toString();
        }
    }
}

CZNIC_WIDGET.measure_speed = function(action, bytes_to_transfer, protocol) {
    /*
     * 1. send/download x bytes to/from uri & start timer
     * 2. on success - call succ handler
     * 	succ handler:
     * 		1. measure the time
     * 		2. store the time and amount into result object
     * 3. on error - err handler
     * 		1. get the reason of failure
     * 		2. store the reason into result object
     * 4. on complete - decide if run more tests
     */

    if (action == CZNIC_WIDGET.UPLOAD) {
        /*
         * When doing an upload test, bytes_to_transfer is not a number.
         * but predefined string of some length.
         */
        bytes_to_transfer = bytes_to_transfer.length;
    }

    var uri = CZNIC_WIDGET.construct_uri(action, bytes_to_transfer, protocol);

    //set the timer (will be started later)
    var d = new Date;
    var test_start = 0;

    //prepare callback, use closure to bind the start time
    var succ_callback = function(json, x, y, z) {
        var d = new Date();
        var test_end = d.getTime()
        var diff = test_end - test_start;
        if (action == CZNIC_WIDGET.UPLOAD) {
            // for upload, subtract the size of response - there could be some
            CZNIC_WIDGET.update_speed_result(action, "success", diff, (bytes_to_transfer - json.length), protocol);
        } else {
            CZNIC_WIDGET.update_speed_result(action, "success", diff, bytes_to_transfer, protocol);
        }
    }

    var fail_callback = function(xOptions, textStatus) {
        CZNIC_WIDGET.update_speed_result(action, textStatus, 0, 0, protocol);
        CZNIC_WIDGET.update_test_result(action + protocol, xOptions);
    }

    var complete_callback = function(xOptions, textStatus) {
        CZNIC_WIDGET.query_complete_handler(action, protocol);
    }
    if (action == CZNIC_WIDGET.DOWNLOAD) {
        // send request
        $.jsonp({
            "url": uri,
            "cache": false,
            "pageCache": false,
            "timeout": 30000,
            "success": succ_callback,
            "error": fail_callback,
            "complete": complete_callback
        });
    }
    // for Upload test, use POST (CORS needs to be available)
    else {
        var fill = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL];
        if (protocol == CZNIC_WIDGET.IP6) {
            fill = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL_V6];
        }
        $.ajax({
            "url": uri,
            "cache": false,
            "type": 'POST',
            "data": fill,
            "timeout": 30000,
            "success": succ_callback,
            "error": fail_callback,
            "complete": complete_callback
        });
    }
    // and start the timer
    test_start = d.getTime();
}

CZNIC_WIDGET.get_best_test_on_current_level = function(action, protocol) {
    var lower_index = 0;
    var upper_index = 0;
    var arr = [];
    if (protocol == CZNIC_WIDGET.IP4) {
        lower_index = CZNIC_WIDGET.DOWN_TEST_LEVEL * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
        upper_index = (CZNIC_WIDGET.DOWN_TEST_LEVEL + 1) * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
        arr = CZNIC_WIDGET.download_results;
        if (action == CZNIC_WIDGET.UPLOAD) {
            // set diff boundaries for upload test
            lower_index = CZNIC_WIDGET.UP_TEST_LEVEL * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
            upper_index = (CZNIC_WIDGET.UP_TEST_LEVEL + 1) * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
            arr = CZNIC_WIDGET.upload_results;
        }
    } else {
        lower_index = CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
        upper_index = (CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 + 1) * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
        arr = CZNIC_WIDGET.download_results_v6;
        if (action == CZNIC_WIDGET.UPLOAD) {
            // set diff boundaries for upload test
            lower_index = CZNIC_WIDGET.UP_TEST_LEVEL_V6 * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
            upper_index = (CZNIC_WIDGET.UP_TEST_LEVEL_V6 + 1) * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
            arr = CZNIC_WIDGET.upload_results_v6;
        }
    }
    // dummy test, time big enough to be sure that real will be lower
    var best_test = {
        time: 99999
    };
    if (arr == undefined) {
        var x = 1;
    }
    for (var i = lower_index; i < upper_index; i++) {
        if (arr[i].time < best_test.time) {
            best_test = arr[i];
        };
    }
    return best_test;
}

CZNIC_WIDGET.upload_test_handler = function() {
    /*
     * Writes results of upload test into html or asks for
     * another measurement.
     */
    var expected_cnt = (CZNIC_WIDGET.UP_TEST_LEVEL + 1) * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
    if (CZNIC_WIDGET.upload_results.length < expected_cnt) {
        var size = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL];
        CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.UPLOAD, size, CZNIC_WIDGET.IP4);
    } else {
        var best_test = CZNIC_WIDGET.get_best_test_on_current_level(CZNIC_WIDGET.UPLOAD,
            CZNIC_WIDGET.IP4);
        if ((best_test.time < CZNIC_WIDGET.UP_TEST_TIME_LIMIT) &&
            (CZNIC_WIDGET.UP_TEST_LEVEL < CZNIC_WIDGET.UP_TEST_MAX_LEVEL)) {
            // test longer data
            CZNIC_WIDGET.UP_TEST_LEVEL++;
            var new_size = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL];
            //CZNIC_WIDGET.debug("Level up: Measuring: up  -> " + new_size.length);
            CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.UPLOAD, new_size, CZNIC_WIDGET.IP4);
        } else {
            var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.UP_INFO_DIV);

            var tot_bytes = best_test.size;
            var tot_time = best_test.time - CZNIC_WIDGET.REF_RTT;
            var speed_calc = CZNIC_WIDGET.calculate_speed(tot_bytes, tot_time);
            var speed = speed_calc.speed;
            var unit = speed_calc.unit;

            var timing_info = tot_bytes + " byte / " + tot_time + " ms"; // rtt:" + CZNIC_WIDGET.REF_RTT;

            CZNIC_WIDGET.debug("U4 test: " + timing_info + " " + speed + " " + unit);

            var info_span;
            if (speed == 0) {
                info_span = $('<span title="' + CZNIC_WIDGET.gettext(CZNIC_WIDGET.NO_CORS_MSG) + '">' + 'N/A </span>');
            } else {
                info_span = $('<span data-bytes="' + tot_bytes + '" data-time="' + tot_time + '" title="' + timing_info + '">' + speed + ' <span class="unit">' + unit + '</span></span>');
            }
            html_target.empty();
            html_target.append(info_span);

            CZNIC_WIDGET.hide_loading(CZNIC_WIDGET.IP4);


            CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP4_UP_TEST_ID, {
                'speed': speed,
                'unit': unit
            });
            // call upload test with ipv6
            //window.frames[0].CZNIC_WIDGET.test_upload_speed_v6();
            // upload test 4 done, start testing download ip6
            CZNIC_WIDGET.test_download_speed_v6();
        }
    }
}

CZNIC_WIDGET.upload_test_handler_v6 = function() {
    /*
     * Writes results of upload test into html or asks for
     * another measurement.
     */
    var expected_cnt = (CZNIC_WIDGET.UP_TEST_LEVEL_V6 + 1) * CZNIC_WIDGET.UP_TEST_MAX_REQ_PER_LEVEL;
    if (CZNIC_WIDGET.upload_results_v6.length < expected_cnt) {
        var size = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL_V6];
        CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.UPLOAD, size, CZNIC_WIDGET.IP6);
    } else {
        var best_test = CZNIC_WIDGET.get_best_test_on_current_level(CZNIC_WIDGET.UPLOAD,
            CZNIC_WIDGET.IP6);
        if ((best_test.time < CZNIC_WIDGET.UP_TEST_TIME_LIMIT) &&
            (CZNIC_WIDGET.UP_TEST_LEVEL_V6 < CZNIC_WIDGET.UP_TEST_MAX_LEVEL)) {
            // test longer data
            CZNIC_WIDGET.UP_TEST_LEVEL_V6++;
            var new_size = CZNIC_WIDGET.UP_TEST_SIZES[CZNIC_WIDGET.UP_TEST_LEVEL_V6];
            CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.UPLOAD, new_size, CZNIC_WIDGET.IP6);
        } else {
            var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.UP_INFO_DIV_V6);

            var tot_bytes = best_test.size;
            var tot_time = best_test.time - CZNIC_WIDGET.REF_RTT_V6;
            var speed_calc = CZNIC_WIDGET.calculate_speed(tot_bytes, tot_time);
            var speed = speed_calc.speed;
            var unit = speed_calc.unit;

            var timing_info = tot_bytes + " byte / " + tot_time + " ms"; // rtt:" + CZNIC_WIDGET.REF_RTT_V6;

            CZNIC_WIDGET.debug("U6 test: " + timing_info + " " + speed + " " + unit);

            var info_span;
            if (speed == 0) {
                // reason why the v6 upload test failed is either CORS or no IPv6
                var reason = CZNIC_WIDGET.gettext(CZNIC_WIDGET.NO_CORS_MSG);
                if (!CZNIC_WIDGET.IPV6_OK) {
                    reason = CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_SPEED_FAIL_MSG);
                }
                info_span = $('<span title="' + reason + '">' + "N/A </span>");
            } else {
                info_span = $('<span data-bytes="' + tot_bytes + '" data-time="' + tot_time + '" title="' + timing_info + '">' + speed + ' <span class="unit">' + unit + '</span></span>');
            }
            html_target.empty();
            html_target.append(info_span);

            CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP6_UP_TEST_ID, {
                'speed': speed,
                'unit': unit
            });

            CZNIC_WIDGET.hide_loading(CZNIC_WIDGET.IP6);
            return;
        }
    }
}

CZNIC_WIDGET.download_test_handler = function() {
    /*
     * Writes results of download test into html or asks for
     * another measurement.
     */
    var expected_cnt = (CZNIC_WIDGET.DOWN_TEST_LEVEL + 1) * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
    if (CZNIC_WIDGET.download_results.length < expected_cnt) {
        var size = CZNIC_WIDGET.DOWN_TEST_SIZES[CZNIC_WIDGET.DOWN_TEST_LEVEL];
        CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.DOWNLOAD, size, CZNIC_WIDGET.IP4);
    } else {
        var best_test = CZNIC_WIDGET.get_best_test_on_current_level(CZNIC_WIDGET.DOWNLOAD,
            CZNIC_WIDGET.IP4);
        if ((best_test.time < CZNIC_WIDGET.DOWN_TEST_TIME_LIMIT) &&
            (CZNIC_WIDGET.DOWN_TEST_LEVEL < CZNIC_WIDGET.DOWN_TEST_MAX_LEVEL)) {
            // test longer data
            CZNIC_WIDGET.DOWN_TEST_LEVEL++;
            var new_size = CZNIC_WIDGET.DOWN_TEST_SIZES[CZNIC_WIDGET.DOWN_TEST_LEVEL];
            CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.DOWNLOAD, new_size, CZNIC_WIDGET.IP4);
        } else {
            var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.DOWN_INFO_DIV);

            var time = best_test.time - CZNIC_WIDGET.REF_RTT;

            var timing_info = best_test.size + " byte / " + time + " ms"; // ref_rtt:" +
            /*CZNIC_WIDGET.REF_RTT +
												" real_time " +
												best_test.time;*/
            var speed_calc = CZNIC_WIDGET.calculate_speed(best_test.size,
                best_test.time - CZNIC_WIDGET.REF_RTT
            );
            var speed = speed_calc.speed;
            var unit = speed_calc.unit;

            CZNIC_WIDGET.debug("D4 test: " + timing_info + " " + speed + " " + unit);

            var info_span = $('<span data-bytes="' + best_test.size + '" data-time="' + time + '" title="' + timing_info + '">' + speed + ' <span class="unit">' + unit + '</span></span>');

            html_target.empty();
            html_target.append(info_span);
            CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP4_DOWN_TEST_ID, {
                'speed': speed,
                'unit': unit
            });
            // download test with ipv4 done, start testing upload with ipv4
            CZNIC_WIDGET.test_upload_speed();
        }
    }
}

CZNIC_WIDGET.download_test_handler_v6 = function() {
    /*
     * Writes results of download test into html or asks for
     * another measurement.
     */
    var expected_cnt = (CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 + 1) * CZNIC_WIDGET.DOWN_TEST_MAX_REQ_PER_LEVEL;
    if (CZNIC_WIDGET.download_results_v6.length < expected_cnt) {
        var size = CZNIC_WIDGET.DOWN_TEST_SIZES[CZNIC_WIDGET.DOWN_TEST_LEVEL_V6];
        CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.DOWNLOAD, size, CZNIC_WIDGET.IP6);
    } else {
        var best_test = CZNIC_WIDGET.get_best_test_on_current_level(CZNIC_WIDGET.DOWNLOAD,
            CZNIC_WIDGET.IP6);
        if ((best_test.time < CZNIC_WIDGET.DOWN_TEST_TIME_LIMIT) &&
            (CZNIC_WIDGET.DOWN_TEST_LEVEL_V6 < CZNIC_WIDGET.DOWN_TEST_MAX_LEVEL)) {
            // test longer data
            CZNIC_WIDGET.DOWN_TEST_LEVEL_V6++;
            var new_size = CZNIC_WIDGET.DOWN_TEST_SIZES[CZNIC_WIDGET.DOWN_TEST_LEVEL_V6];
            CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.DOWNLOAD, new_size, CZNIC_WIDGET.IP6);
        } else {
            var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.DOWN_INFO_DIV_V6);

            var time = best_test.time - CZNIC_WIDGET.REF_RTT_V6;

            var timing_info = best_test.size + " byte / " + time + " ms"; // ref_rtt:" +
            /*CZNIC_WIDGET.REF_RTT_V6 +
												" real_time " +
												best_test.time;*/
            var speed_calc = CZNIC_WIDGET.calculate_speed(best_test.size,
                best_test.time - CZNIC_WIDGET.REF_RTT_V6
            );
            var speed = speed_calc.speed;
            var unit = speed_calc.unit;

            CZNIC_WIDGET.debug("D6 test: " + timing_info + " " + speed + " " + unit);

            var info_span = $('<span data-bytes="' + best_test.size + '" data-time="' + time + '" title="' + timing_info + '">' + speed + ' <span class="unit">' + unit + '</span></span>');

            html_target.empty();
            html_target.append(info_span);

            CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP6_DOWN_TEST_ID, {
                'speed': speed,
                'unit': unit
            });
            // download test with ipv6 done, start testing upload with ipv4
            //parent.CZNIC_WIDGET.test_upload_speed();
            CZNIC_WIDGET.test_upload_speed_v6();
        }
    }
}


CZNIC_WIDGET.query_complete_handler = function(action, protocol) {
    /*
     * Just decides which handler is called.
     */
    if (action == CZNIC_WIDGET.UPLOAD) {
        if (protocol == CZNIC_WIDGET.IP4) {
            CZNIC_WIDGET.upload_test_handler();
        } else {
            CZNIC_WIDGET.upload_test_handler_v6();
        }
    } else {
        if (protocol == CZNIC_WIDGET.IP4) {
            CZNIC_WIDGET.download_test_handler();
        } else {
            CZNIC_WIDGET.download_test_handler_v6();
        }
    }
}

CZNIC_WIDGET.run_download_test = function(protocol) {
    /*
     * Starts testing of download
     */
    var size = CZNIC_WIDGET.DOWN_TEST_SIZES[0];
    CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.DOWNLOAD, size, protocol);
}

CZNIC_WIDGET.run_upload_test = function(protocol) {
    /*
     * Starts testing of upload
     */
    var size = CZNIC_WIDGET.UP_TEST_SIZES[0];
    CZNIC_WIDGET.measure_speed(CZNIC_WIDGET.UPLOAD, size, protocol);
}

CZNIC_WIDGET.set_reference_rtt = function(action, protocol) {
    /*
     * Sends several empty requests to get the roundtrip
     * time of an empty request.
     * This value will be subtracted from the rtt of
     * requests with data.
     */
    //set the timer (will be started later)
    var d = new Date;
    var test_start = 0;

    //prepare callback, use closure to bind the start time
    var succ_callback = function(json) {
        var d = new Date();
        var test_end = d.getTime()
        var diff = test_end - test_start;
        if (protocol == CZNIC_WIDGET.IP6) {
            CZNIC_WIDGET.ref_rtts_v6.push(diff);
        } else {
            CZNIC_WIDGET.ref_rtts.push(diff);
        }
    }

    var fail_callback = function(xOptions, textStatus) {
        CZNIC_WIDGET.ERR_EMPTY_RTTS++;
    }

    var complete_callback = function(xOptions, textStatus) {
        if (protocol == CZNIC_WIDGET.IP4) {
            // there were too many errors, surrender
            if (CZNIC_WIDGET.ERR_EMPTY_RTTS > CZNIC_WIDGET.MAX_ERR_EMPTY_RTTS) {
                return;
            }
            // we have enough reference results
            if (CZNIC_WIDGET.ref_rtts.length >= CZNIC_WIDGET.MAX_NUM_OF_EMPTY_REQ) {
                if (CZNIC_WIDGET.REF_RTT == 0) {
                    var count = CZNIC_WIDGET.ref_rtts.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += CZNIC_WIDGET.ref_rtts[i]
                    }
                    var avg = Math.round(sum / count);
                    CZNIC_WIDGET.REF_RTT = avg;
                }
                // reference rtt is set, start 'real' testing
                if (action == CZNIC_WIDGET.DOWNLOAD) {
                    CZNIC_WIDGET.run_download_test(protocol);
                } else {
                    CZNIC_WIDGET.run_upload_test(protocol);
                }
                return;
            } else {
                CZNIC_WIDGET.set_reference_rtt(action, protocol);
            }
        } else {
            // there were too many errors, surrender
            if (CZNIC_WIDGET.ERR_EMPTY_RTTS_V6 > CZNIC_WIDGET.MAX_ERR_EMPTY_RTTS_V6) {
                return;
            }
            // we have enough reference results
            if (CZNIC_WIDGET.ref_rtts_v6.length >= CZNIC_WIDGET.MAX_NUM_OF_EMPTY_REQ) {
                if (CZNIC_WIDGET.REF_RTT_V6 == 0) {
                    var count = CZNIC_WIDGET.ref_rtts_v6.length;
                    var sum = 0;
                    for (var i = 0; i < count; i++) {
                        sum += CZNIC_WIDGET.ref_rtts_v6[i]
                    }
                    var avg = Math.round(sum / count);
                    CZNIC_WIDGET.REF_RTT_V6 = avg;
                }
                // reference rtt is set, start 'real' testing
                if (action == CZNIC_WIDGET.DOWNLOAD) {
                    CZNIC_WIDGET.run_download_test(protocol);
                } else {
                    CZNIC_WIDGET.run_upload_test(protocol);
                }
                return;
            } else {
                CZNIC_WIDGET.set_reference_rtt(action, protocol);
            }
        }
    }

    var ref_uri = (protocol == CZNIC_WIDGET.IP4) ? CZNIC_WIDGET.REF_URI : CZNIC_WIDGET.REF_URI_V6;
    // send request
    $.jsonp({
        "url": ref_uri,
        "cache": false,
        "pageCache": false,
        "timeout": 10000,
        "success": succ_callback,
        "error": fail_callback,
        "complete": complete_callback
    });

    // and start the timer
    test_start = d.getTime();
}

CZNIC_WIDGET.test_download_speed = function() {
    // set the 'reference' rtt and start tests
    CZNIC_WIDGET.set_reference_rtt(CZNIC_WIDGET.DOWNLOAD, CZNIC_WIDGET.IP4);

}

CZNIC_WIDGET.test_upload_speed = function() {
    var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.UP_INFO_DIV);
    var test_upload = $("#widget_test_upload:checked", CZNIC_WIDGET.widget_context);
    if (!test_upload.length) {
        html_target.html("N/A");
        html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_SPEED_FAIL_MSG));
        CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP4_UP_TEST_ID, {'speed': 0, 'unit': 'untested'});
        // console.log(test)
        // run download 6 test
        CZNIC_WIDGET.test_download_speed_v6();
        return;
    }
    // 'reference' rtt is already set, run the tests
    CZNIC_WIDGET.run_upload_test(CZNIC_WIDGET.IP4);
}


// local constants for waitnig until IPv6 test finishes
CZNIC_WIDGET.WAIT_FOR_IPV6_COUNT = 0;
CZNIC_WIDGET.WAIT_FOR_IPV6_LIMIT = 5;
CZNIC_WIDGET.WAIT_FOR_IPV6_MILIS = 500;

CZNIC_WIDGET.test_download_speed_v6 = function() {
    if (!CZNIC_WIDGET.IPV6_OK) {
        if (CZNIC_WIDGET.WAIT_FOR_IPV6_COUNT >= CZNIC_WIDGET.WAIT_FOR_IPV6_LIMIT) {
            var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.DOWN_INFO_DIV_V6);
            html_target.html("N/A");
            html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_SPEED_FAIL_MSG));
            // IPv6 is down, go to IPv6 upload test (will also ends like this)
            CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP6_DOWN_TEST_ID, {
                'speed': 0,
                'unit': 'untested'
            });
            CZNIC_WIDGET.test_upload_speed_v6();
            return;
        }
        CZNIC_WIDGET.WAIT_FOR_IPV6_COUNT++;
        setTimeout('CZNIC_WIDGET.test_download_speed_v6()',
            CZNIC_WIDGET.WAIT_FOR_IPV6_MILIS);
    } else {
        CZNIC_WIDGET.WAIT_FOR_IPV6_COUNT = 0;
        CZNIC_WIDGET.set_reference_rtt(CZNIC_WIDGET.DOWNLOAD, CZNIC_WIDGET.IP6);

    }
}

CZNIC_WIDGET.test_upload_speed_v6 = function() {
    var html_target = CZNIC_WIDGET.get_result_placeholder(CZNIC_WIDGET.UP_INFO_DIV_V6);
    var test_upload = $("#widget_test_upload:checked", document); //, CZNIC_WIDGET.widget_context);
    if (!test_upload.length) {
        html_target.html("N/A");
        html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_SPEED_FAIL_MSG));
        CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP6_UP_TEST_ID, {
            'speed': 0,
            'unit': 'untested'
        });
        return;
    }
    if (!CZNIC_WIDGET.IPV6_OK) {
        $(".loader-anim.speed-ipv6").addClass("hidden");
        html_target.html("N/A");
        html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_SPEED_FAIL_MSG));
        CZNIC_WIDGET.update_test_result(CZNIC_WIDGET.IP6_UP_TEST_ID, {
            'speed': 0,
            'unit': 'untested'
        });
        CZNIC_WIDGET.hide_loading(CZNIC_WIDGET.IP6);
        return;
    }
    // 'reference' rtt is already set, run the tests
    CZNIC_WIDGET.run_upload_test(CZNIC_WIDGET.IP6);
}

/*
 * ========================================
 * Entry point for speed tests.
 * Runs Ipv4 download test, then IPv6
 * download test (if v6 is available),
 * then Ipv4 upload test (if user wants to)
 * and finally Ipv6 upload test (also if
 * available).
 * ========================================
 */
CZNIC_WIDGET.run_speed_tests = function() {
    CZNIC_WIDGET.test_download_speed();
    // after this test ends, runs the test with ipv6, then the upload tests
}
