/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * This script contains implementation of the tests.
 */

// results of the tests
CZNIC_WIDGET.test_results = [];


CZNIC_WIDGET.ipv6_dualstack_test = function (test_id, html_id, url){
  var html_target = CZNIC_WIDGET.get_result_placeholder(html_id);
	var success = function(ipinfo){
	      var info = "IP: " + ipinfo.ip;
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
                // console.log("ipv6_dualstack_test: " + JSON.stringify(ipinfo))
				html_target.empty();
				html_target.append(info_el);
	      CZNIC_WIDGET.update_test_result(test_id, true);
  }
  var error = function(e){
	      var info = "Test failed";
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
				html_target.empty();
				html_target.append(info_el);
	      CZNIC_WIDGET.update_test_result(test_id, false);
  }
  CZNIC_WIDGET.generic_test(url, success, error);
}


CZNIC_WIDGET.ipv6_jumbo_test = function (test_id, html_id, url){
	var html_target = CZNIC_WIDGET.get_result_placeholder(html_id);
	var success = function (ipinfo) {
				var info = "IP: "+ipinfo.ip + " type: "+ipinfo.type;
				info = info + " padding len "+ ipinfo.padding.length;
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
                // console.log("ipv6_jumbo_test: " + JSON.stringify(ipinfo))
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, true);
	}

	var error = function(e){
				var info = "Test failed";
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, false);
	}

	CZNIC_WIDGET.generic_test(url, success, error);
}

CZNIC_WIDGET.ipv6_dns_test = function (test_id, html_id, url){
	var html_target = CZNIC_WIDGET.get_result_placeholder(html_id);
	var success = function (ipinfo) {
				var info = "IP: "+ipinfo.ip + " type: "+ipinfo.type;
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
                // console.log("ipv6_dns_test: " + JSON.stringify(ipinfo))
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.IPV6_OK = true;
				CZNIC_WIDGET.update_test_result(test_id, true);
	}
	var error = function(e){
				var info = "Test failed";
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, false);
	}

	CZNIC_WIDGET.generic_test(url, success, error);
}

CZNIC_WIDGET.ipv6_nodns_test = function (test_id, html_id, url){
	var html_target = CZNIC_WIDGET.get_result_placeholder(html_id);
	var success = function (ipinfo) {
				var info = "IP: "+ipinfo.ip + " type: "+ipinfo.type;
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
                // console.log("ipv6_nodns_test: " + JSON.stringify(ipinfo))
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, true);
			}
	var error =function(e){
				var info = "Test failed";
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, true);
			}
	CZNIC_WIDGET.generic_test(url, success, error);

}

CZNIC_WIDGET.ipv4_only_test = function (test_id, html_id, url){
	var html_target = CZNIC_WIDGET.get_result_placeholder(html_id);
	var success = function (ipinfo) {
				var info = ipinfo.ip + " type: " +ipinfo.type;
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
                // console.log("ipv4_only_test: " + JSON.stringify(ipinfo))
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, true);
	}
	var error =  function(e){
				var info = "Test failed";
				var info_el = CZNIC_WIDGET.create_img_with_tooltip(info);
				html_target.empty();
				html_target.append(info_el);
				CZNIC_WIDGET.update_test_result(test_id, false);
	}

	CZNIC_WIDGET.generic_test(url, success, error);
}

CZNIC_WIDGET.dnssec_test = function (test_id, html_id, url){
	var html_target = $("#"+html_id);
	var success = function (ipinfo) {
				var img = CZNIC_WIDGET.IMG_DNSSEC_FAIL;
				html_target.css("background-image", 'url("'+img+'")');
				html_target.css("color", CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR);
				$("#dnssec_info").attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.DNSSEC_FAIL_MSG));
                // console.log("dnssec_test: " + JSON.stringify(ipinfo))
				// link will be calculated later
				//$("#dnssec_label").wrapInner('<a target="_top" href="'+CZNIC_WIDGET.DNSSEC_FAIL_LINK+'" />')
				CZNIC_WIDGET.update_test_result(test_id, false);
				// remove the rotator
				$("#dnssec_info .rotator_img").remove();
				$("#dnssec_info .text").css("margin-left", CZNIC_WIDGET.CSS_DNSSEC_IP6_TEXT_LM);
			}
	var error =  function(e){
				var img = CZNIC_WIDGET.IMG_DNSSEC_OK;
				html_target.css("background-image", 'url("'+img+'")');
				html_target.css("color", CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR);
				$("#dnssec_info").attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.DNSSEC_OK_MSG));
				// link will be calculated later
				//$("#dnssec_label").wrapInner('<a target="_top" href="'+CZNIC_WIDGET.DNSSEC_OK_LINK+'" />')
				CZNIC_WIDGET.update_test_result(test_id, true);
				// remove the rotator
				$("#dnssec_info .rotator_img").remove();
				$("#dnssec_info .text").css("margin-left", CZNIC_WIDGET.CSS_DNSSEC_IP6_TEXT_LM);
			}

	CZNIC_WIDGET.generic_test(url, success, error);

}

CZNIC_WIDGET.generic_test = function (test_url, ajax_success_callback, ajax_error_callback){
	$.jsonp({
			"url": test_url,
			"cache": false,
			"pageCache": false,
			"timeout": 30000,
			"success": ajax_success_callback,
			"error": ajax_error_callback
	})
}




