/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Tooltips and labels - multilang. - array of translations, 
 * app chooses the correct one.
 * Currently [cs, en, fr, es, eu]
 * 
 * If You want to add a new translation:
 * 1. add language code to the SUPPORTED_LANGS list
 * 2. add the translation of each term to the dictionary, use lang code as key
 * 
 * Try to keep HEADER_LABEL under (approx.) 27 characters,
 * DNSSEC_LABEL and IPV6_LABEL under (approx.) 18 characters and 
 * CONSPEED_LABEL under (approx.) 15 characters.
 * 
 * If You want to see how Your translation will look like in the browser, 
 * get the widget src, open widget.html locally and pass it lang setting of the new
 * language. (e.g. file:///home/foobar/ipv6widget/src/widget.html?lang=xx)
 */

CZNIC_WIDGET.SUPPORTED_LANGS = ["cs", "en", "fr", "es", "eu", "bg", "ru", "hu", "pl", "sk"];

CZNIC_WIDGET.IP6_GREEN_MSG = {
cs: "Váš počítač používá pro přístup k internetovým službám a zdrojům protokol IPv6", 
en: "You are using IPv6", 
fr: "Vous utilisez IPv6",
es: "Usted está utilizando IPv6",
eu: "IPv6 erabiltzen ari zara",
bg: "Вие използвате IPv6",
ru: "Вы используете IPv6",
hu: "Használja az IPv6 protokollt",
pl: "Korzystasz z IPv6",
sk: "Váš počítač používa pre prístup k internetovým službám a zdrojom protokol IPv6"
};

CZNIC_WIDGET.IP6_OFF_MSG = {
cs: "Váš počítač nepoužívá pro přístup k internetovým službám a zdrojům protokol IPv6", 
en: "You are not using IPv6",
fr: "Vous n'utilisez pas IPv6",
es: "Usted no está utilizando IPv6",
eu: "Ez zaude IPv6 erabiltzen",
bg: "Вие не използвате IPv6",
ru: "Вы не используете IPv6",
hu: "Nem használja az IPv6 protokollt",
pl: "Nie korzystasz z IPv6",
sk: "Váš počítač nepoužíva pre prístup k internetovým službám a zdrojom protokol IPv6"
};

CZNIC_WIDGET.IP6_RED_MSG = {
cs: "Váš počítač není připraven pro protokol IPv6", 
en: "You are not IPv6 ready", 
fr: "Vous n'êtes pas prêt pour IPv6",
es: "Usted no está preparado para usar IPv6",
eu: "Ez zaude IPv6 erabiltzeko prest",
bg: "Вие не сте готови за IPv6",
ru: "Вы не готовы к IPv6",
hu: "Az Ön gépe nem kész az IPv6 prokoll használatára",
pl: "Nie jesteś przygotowany na IPv6",
sk: "Váš počítač nie je pripravený pre protokol IPv6"
};

CZNIC_WIDGET.DNSSEC_OK_MSG = {
cs: "Váš počítač je při přístupu k internetovým službám a zdrojům zabezpečen technologií DNSSEC", 
en: "You are DNSSEC protected", 
fr: "Vous êtes protégé par DNSSEC",
es: "Usted está protegido por DNSSEC",
eu: "DNSSECen babespean zaude",
bg: "Вие сте защитени от DNSSEC",
ru: "Вы защищены технологией DNSSEC",
hu: "Az Ön gépe DNSSEC protokoll által védett",
pl: "Jesteś zabezpieczony protokołem DNSSEC",
sk: "Váš počítač je pri prístupe k internetovým službám a zdrojom zabezpečenýtechnológiou DNSSEC"
};

CZNIC_WIDGET.DNSSEC_FAIL_MSG = {
cs: "Váš počítač není při přístupu k internetovým službám a zdrojům zabezpečen technologií DNSSEC", 
en: "You are not DNSSEC protected", 
fr: "Vous n'êtes pas protégé par DNSSEC",
es: "Usted no está protegido por DNSSEC",
eu: "Ez zaude DNSSECen babespean",
bg: "Вие не сте защитени от DNSSEC",
ru: "Вы не защищены технологией DNSSEC",
hu: "Az Ön gépe nem védett a DNSSEC protokoll által",
pl: "Nie jesteś zabezpieczony protokołem DNSSEC",
sk: "Váš počítač nie je pri prístupe k internetovým službám a zdrojom zabezpečný technológiou DNSSEC"
};

CZNIC_WIDGET.IP6_SPEED_FAIL_MSG = {
cs: "Váš počítač nepoužívá protokol IPv6", 
en: "You do not use IPv6", 
fr: "Vous n'utilisez pas IPv6",
es: "Usted no está utilizando IPv6",
eu: "Ez zaude IPv6 erabiltzen",
bg: "Вие не използвате IPv6",
ru: "Вы не используете IPv6",
hu: "Nem használ IPv6 protokollt",
pl: "Nie korzystasz z IPv6",
sk: "Váš počítač nepoužíva protokol IPv6"
};

CZNIC_WIDGET.ERROR_MSG = {
cs: "Vyskytla se chyba", 
en: "An error occured", 
fr: "Une erreur s'est produite",
es: "Se ha producido un error",
eu: "Errore bat gertatu da",
bg: "Възникна грешка",
ru: "Произошла ошибка",
hu: "Hiba történt",
pl: "Nastąpił błąd",
sk: "Vyskytla sa chyba"
};

CZNIC_WIDGET.NO_CORS_MSG = {
cs: "Váš prohlížeč nepodporuje CORS", 
en: "Your browser does not support CORS", 
fr: "Votre navigateur ne support pas CORS",
es: "Su navegador no soporta CORS",
eu: "Zure nabigatzaileak ez du CORS onartzen",
bg: "Вашият браузър не поддържа CORS",
ru: "Ваш браузер не поддерживает CORS",
hu: "Az Ön böngészője nem támogatja a CORS-t",
pl: "Twoja przeglądarka nie wspiera CORS",
sk: "Váš prehliadač nepodporuje CORS"
};

CZNIC_WIDGET.HEADER_LABEL = {
cs: "Vaše připojení k Internetu", 
en: "Your Internet connection", 
fr: "Votre connexion Internet",
es: "Su conexión a Internet",
eu: "Zure Internet konexioa",
bg: "Вашата Интернет връзка",
ru: "Ваше Интернет-соединение",
hu: "Az Ön Internet kapcsolata",
pl: "Twoje połączenie z Internetem",
sk: "Vaše pripojenie k internetu"
};

CZNIC_WIDGET.DNSSEC_LABEL = {
cs: "Chráněno DNSSEC", 
en: "DNSSEC protection", 
fr: "Protection DNSSEC",
es: "Protección DNSSEC",
eu: "DNSSEC babesa",
bg: "DNSSEC защита",
ru: "Защищено DNSSEC",
hu: "DNSSEC által védett",
pl: "Zabezpieczenie DNSSEC",
sk: "Chránené DNSSEC"
};

CZNIC_WIDGET.IPV6_LABEL = {
cs: "Protokol IPv6", 
en: "IPv6 protocol", 
fr: "Protocole IPv6",
es: "Protocolo IPv6",
eu: "IPv6 protokoloa",
bg: "IPv6 протокол",
ru: "Протокол IPv6",
hu: "IPv6 protokoll",
pl: "Protokół IPv6",
sk: "Protokol IPv6"
};

CZNIC_WIDGET.CONSPEED_LABEL = {
cs: "Změřit rychlost", 
en: "Measure speed", 
fr: "Vitesse mesurée",
es: "Test de velocidad",
eu: "Abiadura testa",
bg: "Измери скоростта",
ru: "Измерить скорость",
hu: "Megmérni a sebességet",
pl: "Zmierzyć prędkość",
sk: "Zmerať rýchlosť"
};
