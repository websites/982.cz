/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Constants for upload test - strings with different lengths.
 */

CZNIC_WIDGET.s1kB = "\
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut eget aliquam lacus. Curabitur sagittis orci\
 sed magna varius scelerisque. Sed eget ante quam, eget tincidunt diam. Duis congue risus at velit cons\
 equat sit amet suscipit eros fringilla. Nunc hendrerit elit ut tellus varius ac consequat neque portti\
 tor. Fusce suscipit dolor quis eros tristique vel sodales ante feugiat. Proin luctus, mi id suscipit b\
 ibendum, justo erat venenatis dui, sit amet posuere enim nulla eu nisl. Quisque elementum hendrerit ni\
 si et aliquam. Sed dictum, arcu vitae fermentum cursus, tortor nunc posuere metus, sed rhoncus est nis\
 l a dolor. Sed libero urna, sodales ut mattis ac, hendrerit quis dui. Vestibulum ac turpis tortor. Nam\
  at dui enim, eget sagittis leo. Vivamus tortor quam, porttitor at dapibus eget, ullamcorper ut augue.\
	 Sed mattis hendrerit justo, ut varius leo viverra sed. Sed a rhoncus enim. Sed sodales nulla a risus\
	  hendrerit faucibus. Proin vestibulum fringilla neque, eu convallis eros posuere.\
";

CZNIC_WIDGET.s10kB =  CZNIC_WIDGET.s1kB + CZNIC_WIDGET.s1kB +
											CZNIC_WIDGET.s1kB + CZNIC_WIDGET.s1kB +
											CZNIC_WIDGET.s1kB + CZNIC_WIDGET.s1kB +
											CZNIC_WIDGET.s1kB + CZNIC_WIDGET.s1kB +
											CZNIC_WIDGET.s1kB + CZNIC_WIDGET.s1kB; 
											
CZNIC_WIDGET.s20kB = CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB;

CZNIC_WIDGET.s50kB = CZNIC_WIDGET.s20kB + CZNIC_WIDGET.s20kB + CZNIC_WIDGET.s10kB;

CZNIC_WIDGET.s100kB = CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB +											
											CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB +
											CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB +
											CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB +
											CZNIC_WIDGET.s10kB + CZNIC_WIDGET.s10kB;

CZNIC_WIDGET.s1MB = CZNIC_WIDGET.s100kB + CZNIC_WIDGET.s100kB +											
											CZNIC_WIDGET.s100kB + CZNIC_WIDGET.s100kB +
											CZNIC_WIDGET.s100kB + CZNIC_WIDGET.s100kB +
											CZNIC_WIDGET.s100kB + CZNIC_WIDGET.s100kB +
											CZNIC_WIDGET.s100kB + CZNIC_WIDGET.s100kB;

CZNIC_WIDGET.s2MB = CZNIC_WIDGET.s1MB + CZNIC_WIDGET.s1MB;

CZNIC_WIDGET.s5MB = CZNIC_WIDGET.s2MB + CZNIC_WIDGET.s2MB + CZNIC_WIDGET.s1MB;

CZNIC_WIDGET.s10MB = CZNIC_WIDGET.s5MB + CZNIC_WIDGET.s5MB;