/*  Copyright (C) 2011 CZ.NIC, z.s.p.o. <kontakt@nic.cz>
*
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
 * Script with helper tools used in tests.
 */

// context of the widget - all jQueries have to be done with this context
CZNIC_WIDGET.widget_context = null;


CZNIC_WIDGET.get_result_placeholder = function (html_id){
	return $("#"+html_id+" > span[class='result']", CZNIC_WIDGET.widget_context);
}

/*
 * Finds element that contains widget's html.
 */
CZNIC_WIDGET.find_context_element = function (){
	var el = $("#"+CZNIC_WIDGET.WIDGET_ID);
	if (el.length == 0){
		return null;
	}
	return el;
}

/*
 * Returns string consisting of "size" "str"s.
 */
CZNIC_WIDGET.fill = function (str, size){
	var res = "";
	for (var i=0; i< size; i++){
		res += str;
	}
	return res;
}

/*
 * Looks for the test with test_name.
 * Returns test instance or null.
 */
CZNIC_WIDGET.find_test = function (name){
	var i = CZNIC_WIDGET.test_results.length;
  while (i--) {
    if (CZNIC_WIDGET.test_results[i].name == name) {
      return CZNIC_WIDGET.test_results[i];
    }
  }
  return null;
}

/*
 * Updates test result. If the test with this name is not among
 * tests, creates one.
 */
CZNIC_WIDGET.update_test_result = function (test_name, result_value){
	var test = CZNIC_WIDGET.find_test[test_name];
	if (test == null) {
  	var new_test = {
  		name: test_name,
  		result: result_value
  	};
  	CZNIC_WIDGET.test_results.push(new_test);
  }
  else {
  	test.result = result_value;
  }

	if (!CZNIC_WIDGET.IPV6_OVERALL_UPDATED){
		CZNIC_WIDGET.update_dnssecip6_overall();

	}

}

CZNIC_WIDGET.update_dnssecip6_overall = function(){
	// update ipv6 overall status
	var ip4 = CZNIC_WIDGET.find_test(CZNIC_WIDGET.IP4_TEST_ID);
	var ip6dns = CZNIC_WIDGET.find_test(CZNIC_WIDGET.IP6_DNS_TEST_ID);
	//var ip6nodns = CZNIC_WIDGET.find_test(CZNIC_WIDGET.IP6_NODNS_TEST_ID);
	var ip6jumbo = CZNIC_WIDGET.find_test(CZNIC_WIDGET.IP6_JUMBO_TEST_ID);
	var ip6dualstack = CZNIC_WIDGET.find_test(CZNIC_WIDGET.IP6_DUAL_TEST_ID);
	var dnssec = CZNIC_WIDGET.find_test(CZNIC_WIDGET.DNSSEC_TEST_ID);

	// tests are not done yet
	if ((ip4 == null) ||(ip6dns == null) ||
			(ip6jumbo == null) ||(ip6dualstack == null) || (dnssec == null)){
		return;
	}

	var green = "GREEN";
	var grey = "GREY";
	var red = "RED";

	var dnsseccolor = "";
	if (dnssec.result){
		dnsseccolor = green;
	}
	else{
		dnsseccolor = red;
	}

	var html_target = $("#ip6_overall");
	var ip6color = "";
	var img = "";
	if (ip6dualstack.result){
		if (ip6dns.result){
			// green
			img = CZNIC_WIDGET.IMG_IP6_OK;
			html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_GREEN_MSG));
			ip6color = green;
		}
		else{
			//off
			img = CZNIC_WIDGET.IMG_IP6_OFF;
			html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_OFF_MSG));
			ip6color = grey;
		}
	}
	else{
		// red
		img = CZNIC_WIDGET.IMG_IP6_FAIL;
		html_target.attr("title", CZNIC_WIDGET.gettext(CZNIC_WIDGET.IP6_RED_MSG));
		ip6color = red;
	}

	// html_target.css("background-image", 'url("'+img+'")');
	// html_target.css("color", CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR);
	// $("#ip6_overall .rotator_img").remove();
	// $("#ip6_overall .text").css("margin-left", CZNIC_WIDGET.CSS_DNSSEC_IP6_TEXT_LM);
	CZNIC_WIDGET.IPV6_OVERALL_UPDATED = true;

	//update links at dnssec control and ipv6 control
	var link_prop_name = "IP6_" + ip6color + "_DNSSEC_" + dnsseccolor + "_LINK";
	var targetlink = CZNIC_WIDGET[link_prop_name];
	$('#ipv6_label').wrapInner('<a target="_top" href="'+targetlink+'" />');
	$("#dnssec_label").wrapInner('<a target="_top" href="'+targetlink+'" />');

}


CZNIC_WIDGET.create_img_with_tooltip = function (tooltip_text){
	var info = $('<a title="'+tooltip_text+'">info</a>');
	return info;
}

CZNIC_WIDGET.round_digits = function(number, digits){
	if (digits < 0){
		return number;
	}
	return Math.round( number * Math.pow(10, digits)) / Math.pow(10, digits);
}

CZNIC_WIDGET.debug = function(msg){
	if (CZNIC_WIDGET.DEBUG) {
  	var area = $("#debug");
  	var to_add = '<div>' + msg + '</div>';
  	area.append(to_add);
  }
}

CZNIC_WIDGET.show_loading = function(test_html_id){
	//$('#'+test_html_id+' span[class="result"]', CZNIC_WIDGET.widget_context).html('<img src="'+CZNIC_WIDGET.IMG_LOADER+'"/>');
}

CZNIC_WIDGET.hide_loading = function(protocol){
	if (protocol == CZNIC_WIDGET.IP4) {
		$("#speed_info_v4").css("background-image", 'url("'+CZNIC_WIDGET.IMG_IP4_SPEED_DONE+'")');
  	$("#speed_info_v4 > .rotator_img").hide();
		// $("#speed_info_v4 > .container").css("margin-left", CZNIC_WIDGET.CSS_SPEED_RESULT_CONTAINER_LM);
		// $("#speed_info_v4  .speed_result").css("color", CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR);
  }
	else{
		$("#speed_info_v6").css("background-image", 'url("'+CZNIC_WIDGET.IMG_IP6_SPEED_DONE+'")');
		$("#speed_info_v6 > .rotator_img").hide();
		// $("#speed_info_v6 > .container").css("margin-left", CZNIC_WIDGET.CSS_SPEED_RESULT_CONTAINER_LM);
		// $("#speed_info_v6  .speed_result").css("color", CZNIC_WIDGET.CSS_DEFAULT_TEXT_COLOR);
	}
}


/*
 * returns internationalized text
 */
CZNIC_WIDGET.gettext = function(msg_key){
	// get language setting from html or from the browser
	var lang_setting = $("#language").html();
	if (msg_key[lang_setting]){
		return msg_key[lang_setting];
	}
	else{
		return msg_key[CZNIC_WIDGET.FALLBACK_LANGUAGE];
	}
}
