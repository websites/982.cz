var get_ipv4 = function(url) {
    var success = function(ipinfo) {
        var ipv4 = ipinfo.ip;
        $("#id_ipv4_address").val(ipv4);
        $("#ipv4_address").text(ipv4).removeClass("loading");
    };
    var error = function(e) {
        $("#ipv4_address").text("N/A").removeClass("loading").addClass("failed");
    };

    CZNIC_WIDGET.generic_test(url, success, error);
};

var get_ipv6 = function(url) {
    var success = function(ipinfo) {
        var ipv6 = ipinfo.ip;
        $("#id_ipv6_address").val(ipv6);
        $("#ipv6_address").text(ipv6).removeClass("loading");
        $("#result-ipv6").addClass("on");
        $(".loader-anim.ipv6").addClass("hidden");
        CZNIC_WIDGET.IPV6_OK = true;
    };
    var error = function(e) {
        $(".loader-anim.ipv6").addClass("hidden");
        $("#ipv6_address").text("N/A").removeClass("loading").addClass("failed");
    };

    CZNIC_WIDGET.generic_test(url, success, error);
};

var dnssec_test = function(url) {
    var success = function(ipinfo) {
        $("#id_has_dnssec").prop("checked", false);
        $(".loader-anim.dnssec").addClass("hidden");
        CZNIC_WIDGET.test_download_speed();
    };
    var error = function(e) {
        $("#id_has_dnssec").prop("checked", true);
        $("#result-dnssec").addClass("on");
        $(".loader-anim.dnssec").addClass("hidden");
        CZNIC_WIDGET.test_download_speed();
    };

    CZNIC_WIDGET.generic_test(url, success, error);
};

var www_test = function(url) {
    $.get("/backend?web=" + url, function(data) {
        $("#id_www_ipv6").prop("checked", data.ipv6);
        if(data.ipv6){
            $("#result-www-ipv6").addClass("on");
        }
        $("#id_www_dnssec").prop("checked", data.dnssec);
        if(data.dnssec){
            $("#result-www-dnssec").addClass("on");
        }
        $(".loader-anim.www-dnssec").addClass("hidden");
        $(".loader-anim.www-ipv6").addClass("hidden");
    });
};


CZNIC_WIDGET.generic_test = function(test_url, ajax_success_callback, ajax_error_callback) {
    $.jsonp({
        "url": test_url,
        "cache": false,
        "pageCache": false,
        "timeout": 30000,
        "success": ajax_success_callback,
        "error": ajax_error_callback
    });
};

$(document).ready(function() {
    $("#website-url").text($("#user-web").val());
    $("#button-start-test").click(function(event) {
        CZNIC_WIDGET.test_results = [];
        $("section.test").removeClass("hidden");
        $("#button-start-test").addClass("hidden");
        www_test($("#user-web").val());
        get_ipv4(CZNIC_WIDGET.IP4_ONLY_TEST);
        get_ipv6(CZNIC_WIDGET.IP6_DNS_TEST);
        dnssec_test(CZNIC_WIDGET.DNSSEC_TEST);
    });

    var checkIfFinished = setInterval(function(){
      if($(".loader-anim:not(.hidden)").length === 0 && $(".result span:contains('- - -')").length === 0) {
        $("#button-save").removeAttr('disabled');
      }
    }, 500);


    $("#button-save").click(function(event) {
        if ($("#id_ipv4_address").val() != "N/A"){
            var ipv4_down = (($("#down_info_v4 .result span").data("bytes") * 8 ) /
                             ($("#down_info_v4 .result span").data("time") / 1000) /
                             ( 1024 * 1024 )).toFixed(2);
            var ipv4_up = (($("#up_info_v4 .result span").data("bytes") * 8 ) /
                           ($("#up_info_v4 .result span").data("time") / 1000) /
                           ( 1024 * 1024 )).toFixed(2);

            if (ipv4_down > 0){
                $("#id_ipv4_download").val(ipv4_down);
            }

            if (ipv4_up > 0){
                $("#id_ipv4_upload").val(ipv4_up);
            }
        }

        if ($("#id_ipv6_address").val() != "N/A"){
            var ipv6_down = (($("#down_info_v6 .result span").data("bytes") * 8 ) /
                             ($("#down_info_v6 .result span").data("time") / 1000) /
                             ( 1024 * 1024 )).toFixed(2);
            var ipv6_up = (($("#up_info_v6 .result span").data("bytes") * 8 ) /
                           ($("#up_info_v6 .result span").data("time") / 1000) /
                           ( 1024 * 1024 )).toFixed(2);

            if (ipv6_down > 0){
                $("#id_ipv6_download").val(ipv6_down);
            }

            if (ipv6_up > 0){
                $("#id_ipv6_upload").val(ipv6_up);
            }
        }

        if($("#user-id").val()){
            $("#id_owner").val($("#user-id").val());
            $("#id_www_url").val($("#user-web").val());
        }
    });
});